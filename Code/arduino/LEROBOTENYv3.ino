/* ARDUINO CODE 
  by Noah, last updated April 19 2023
  So far implements:
  - Maxon motor speed commands (Using potentiometer on A0)
  - Maxon motor sensor data reading
  - Polulu 


  MAXON LEFT MOTOR CONNECTED TO MAXON CONTROLLER #



*/ 

#define ULTRASONIC_INTERVAL 40 // Milliseconds between sensor pings (29ms is about the min to avoid cross-sensor echo).
#define ULTRASONIC_NUM 6 // Number of sensors

// #define trigPin1 30
// #define echoPin1 31

// #define trigPin2 32
// #define echoPin2 33

// #define trigPin3 34
// #define echoPin3 35

// #define trigPin4 36
// #define echoPin4 37

// #define trigPin5 38
// #define echoPin5 39

// #define trigPin6 40
// #define echoPin6 41


unsigned long currentTime = 0;

unsigned long ultrasonicTimer[ULTRASONIC_NUM]; // Holds the times when the next ping should happen for each sensor.
unsigned int cm[ULTRASONIC_NUM] = {100,100,100,100,100,100};         // Where the measured ultrasonic distances are stored.
uint8_t currentUltrasonicSensor = 0;          // Keeps track of which sensor is active.
unsigned int  ultrasonicSensors[ULTRASONIC_NUM];

const int ultrasonicTrigPins[ULTRASONIC_NUM] = {30,32,34,36,38,40};
const int ultrasonicEchoPins[ULTRASONIC_NUM] = {31,33,35,37,39,41};



// MAXON CONTROLLER PINS
const int leftMotorEnablePin = 22;
const int leftMotorDirectionPin = 23;
const int leftMotorPwmPin = 10;

const int rightMotorEnablePin = 24;
const int rightMotorDirectionPin = 25;
const int rightMotorPwmPin = 11;

// DC MOTOR DRIVER PINS
const int MotorADirectionPin = 52;
const int MotorAPwmPin = 8;
const int MotorAEncoderPin = 50;

const int MotorBDirectionPin = 53;
const int MotorBPwmPin = 9;
const int MotorBEncoderPin = 51;




// BLABLA
int Speed; 
int SpeedPolulu;
int current_sense_counter = 0;

int ontimeA,offtimeA,dutyA;
float freqA, periodA;
int ontimeB,offtimeB,dutyB;
float freqB, periodB;

long duration, distance, sonicSensor1;

bool obstacle = 0;

bool motorAstuck = 0;
bool motorBstuck = 0;

void SonarSensor();

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600); // Initialize serial communication with computer in order to read potentiometer values


  // SET PINMODE OF ULTRASONIC SENSORS
  for (uint8_t i = 0; i < ULTRASONIC_NUM; i++) { // Set the starting time for each sensor.
      pinMode(ultrasonicTrigPins[i], OUTPUT);
      pinMode(ultrasonicEchoPins[i], INPUT);
  }  

  ultrasonicTimer[0] = millis() + 2000;           // First ping starts at 2000ms
  for (uint8_t i = 1; i < ULTRASONIC_NUM; i++) { // Set the starting time for each sensor.
      ultrasonicTimer[i] = ultrasonicTimer[i - 1] + ULTRASONIC_INTERVAL;
  }

  Serial.print("SETUP : ");
  for(int i = 0; i < ULTRASONIC_NUM; i++) {
    Serial.println(ultrasonicTimer[i]);
  }
  

  
  // SET UP MAXON MOTOR CONTROLLER ********************
  pinMode(leftMotorEnablePin, OUTPUT);
  pinMode(leftMotorDirectionPin, OUTPUT);
  pinMode(leftMotorPwmPin, OUTPUT);
  pinMode(rightMotorEnablePin, OUTPUT);
  pinMode(rightMotorDirectionPin, OUTPUT);
  pinMode(rightMotorPwmPin, OUTPUT);

  digitalWrite(leftMotorEnablePin, HIGH); 
  digitalWrite(leftMotorDirectionPin, LOW); // error when setting up Maxon controller : forward speed when direction set to 0
  digitalWrite(rightMotorEnablePin, HIGH);
  digitalWrite(rightMotorDirectionPin, LOW);

  analogWrite(leftMotorPwmPin,26); // pwm duty cycle set initially to 10% (corresponds to 0 speed)
  analogWrite(rightMotorPwmPin,26); // pwm duty cycle set initially to 10% (corresponds to 0 speed)

  // SET UP DC MOTOR DRIVER *********************
  pinMode(MotorADirectionPin, OUTPUT);
  pinMode(MotorAPwmPin, OUTPUT);
  pinMode(MotorAEncoderPin, INPUT);
  pinMode(MotorBDirectionPin, OUTPUT);
  pinMode(MotorBPwmPin, OUTPUT);
  pinMode(MotorBEncoderPin, INPUT);

  digitalWrite(MotorADirectionPin, LOW);
  digitalWrite(MotorBDirectionPin, HIGH);
  analogWrite(MotorAPwmPin, 125);
  analogWrite(MotorBPwmPin, 125);


  delay(1000); // wait 2 seconds

}








/********************* LOOP *********************/ 

void loop() {
  // put your main code here, to run repeatedly:

  // float volts = analogRead(A3);  // value from sensor * (5/1024)
  // // int distance_ir = 13*pow(volts, -1); // worked out from datasheet graph
  // Serial.print("Data from IR : ");
  // Serial.println(volts); 

  for (uint8_t i = 0; i < ULTRASONIC_NUM; i++) { // Loop through all the sensors.
      if (millis() >= ultrasonicTimer[i]) {         // Is it this sensor's time to ping?
          SonarSensor(ultrasonicTrigPins[i], ultrasonicEchoPins[i]);
          cm[i] = distance;
          Serial.print("Distance with ultrasonic ");
          Serial.print(i);
          Serial.print(" at time ");
          Serial.print(ultrasonicTimer[i]);
          Serial.print(" : ");
          Serial.println(distance);

          ultrasonicTimer[i] += ULTRASONIC_INTERVAL * ULTRASONIC_NUM;  // Set next time this sensor will be pinged.
      }      
  }

  // SonarSensor(trigPin1, echoPin1);
  // sonicSensor1 = distance;
  // Serial.print("Distance with ultrasonic : ");
  // Serial.println(distance);  


  if (cm[2] <= 20 || cm[1] <= 20){
    digitalWrite(rightMotorDirectionPin, LOW);
  }
  else {
    digitalWrite(rightMotorDirectionPin, HIGH);
  }


  if (cm[3] <= 20 || cm[4] <= 20){
    digitalWrite(leftMotorDirectionPin, LOW);
  }
   else {
    digitalWrite(leftMotorDirectionPin, HIGH);
  }






  

  






  // COMMANDS TO MAXON MOTOR CONTROLLER
  Speed = analogRead(A0); // converts analog signal of potentiometer into value between 0 and 1023
  // to communicate with ESCON cntroller, pwm duty cycle must be between 10% and 90%
  //Serial.println(Speed);


  analogWrite(leftMotorPwmPin, map(Speed, 0, 1023, 26, 230));
  analogWrite(rightMotorPwmPin , map(Speed, 0, 1023, 26, 230)); // desired speed sent as pwm to ESCON controller

  // COMMANDS TO DC MOTOR CONTROLLER
  analogWrite (MotorAPwmPin,map(Speed, 0, 1023, 0, 100));      //PWM Speed Control   
  analogWrite (MotorBPwmPin,map(Speed, 0, 1023, 0, 100)); 



  // MEASURE POLULU MOTOR SPEED

  ontimeA = pulseIn(MotorAEncoderPin,HIGH, 500000);
  offtimeA = pulseIn(MotorAEncoderPin,LOW, 500000);
  periodA = ontimeA+offtimeA;
  freqA =   1000000.0/periodA;
  dutyA = (ontimeA/periodA)*100;
  Serial.println(freqA);
  Serial.println(dutyA);
  Serial.println(periodA);
  
  if (freqA < 800 || freqA > 5000) {
    if (motorAstuck == 0){
      Serial.println("Motor A stuck");
      digitalWrite(MotorADirectionPin, HIGH);
      digitalWrite(MotorBDirectionPin, LOW);
      motorAstuck = 1;
    }


  }
  else {
    if (motorAstuck == 1){
      digitalWrite(MotorADirectionPin, LOW);
      digitalWrite(MotorBDirectionPin, HIGH);
      motorAstuck = 0;
    }

  }








  
  
  delay(100); // makes things more fluid

}





void SonarSensor(int trigPin,int echoPin)
{
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
  duration = pulseIn(echoPin, HIGH);
  distance = duration/58.2;
}

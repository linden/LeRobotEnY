// Latest changes : 
// raspberry pi must send request for arduino to send data
// servo motor for unloading
// added delay when reversing direction of polulu motors (works better)

// NOTE : depending on ULTRASONIC_INTERVAL value, get more or less accurates values from sensors (need to tune at the end)
// NOTE : need to send command to Polulu motors for motors to start turning


/* ARDUINO CODE 
  by Noah, last updated April 19 2023
  So far implements:
  - Maxon motor speed commands (Using potentiometer on A0)
  - Maxon motor sensor data reading
  - Polulu control and encoder reading (direction reversed if motorA stuck)

  - Serial communication with Raspberry pi
    Motor commands : "m+99+00"
    Unloading command : "u"
    Emergency STOP command : "s"
    Raspberry pi requests data : "d"
    Polulu motor command : "p0" to turn off (default), "p1" to turn on



  WARNING only left brosse motor (motorA) detect if duplo gets stuck instead of both motors detecting
    (but turns out works better)

  WARNING ultrasonic sensor sometimes ouptut a value of 0 when not detecting any obstacles
    (may need to implement filtering later)




  MAXON LEFT MOTOR CONNECTED TO MAXON CONTROLLER # 4



*/ 

#define ULTRASONIC_INTERVAL 25 // Milliseconds between sensor pings (29ms is about the min to avoid cross-sensor echo).
#define ULTRASONIC_NUM 6 // Number of sensors

#include <Servo.h>


unsigned long currentTime = 0;

unsigned long ultrasonicTimer[ULTRASONIC_NUM]; // Holds the times when the next ping should happen for each sensor.
unsigned int cm[ULTRASONIC_NUM] = {100,100,100,100,100,100};         // Where the measured ultrasonic distances are stored.
//uint8_t currentUltrasonicSensor = 0;          // Keeps track of which sensor is active.
//unsigned int  ultrasonicSensors[ULTRASONIC_NUM];


// ULTRASONIC PINS
const int ultrasonicTrigPins[ULTRASONIC_NUM] = {30,32,34,36,38,40};
const int ultrasonicEchoPins[ULTRASONIC_NUM] = {31,33,35,37,39,41};

// SERVOMOTOR
Servo bigAssServo;
const int bigAssServoPin = 2;
int pos = 5;


// MAXON CONTROLLER PINS
const int leftMotorEnablePin = 22;
const int leftMotorDirectionPin = 23;
const int leftMotorPwmPin = 10;

const int rightMotorEnablePin = 24;
const int rightMotorDirectionPin = 25;
const int rightMotorPwmPin = 11;

// DC MOTOR DRIVER PINS
const int MotorADirectionPin = 52;
const int MotorAPwmPin = 8;
const int MotorAEncoderPin = 50;

const int MotorBDirectionPin = 53;
const int MotorBPwmPin = 9;
const int MotorBEncoderPin = 51;

bool polulusOn = 0;

int batteryLevel = 0;



// Speed read by potentiometer *************************** REMOVE LATER
int Speed; 

// Variables for brush dc motors
int SpeedPolulu;
int current_sense_counter = 0;

int ontimeA,offtimeA,dutyA;
float freqA, periodA;
int ontimeB,offtimeB,dutyB;
float freqB, periodB;

// Variables for ultrasonic sensors
long duration, distance, sonicSensor1;

// Communication with Raspberry Pi : 
String msg = "";
int forward_speed = 99; // WARNING value between -99 and 99
int angular_speed = 0; // WARNING value between -99 and 99

int left_speed = 0; // WARNING value between -198 and 198
int right_speed = 0; // WARNING value between -198 and 198

// BLABLA
bool obstacle = 0;
bool motorAstuck = 0;
bool motorBstuck = 0;

// FUNCTIONS
void SonarSensor();
void updateMotorCommands();
void sendData();
void readSerialPort();
void sensorAvoidance();
void updatePoluluCommands();
void moveServo();


void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600); // Initialize serial communication with computer

  batteryLevel = analogRead(A15);
  Serial.print("Battery Level : ");
  Serial.println(batteryLevel);


  // SET PINMODE OF ULTRASONIC SENSORS
  for (uint8_t i = 0; i < ULTRASONIC_NUM; i++) { // Set the starting time for each sensor.
      pinMode(ultrasonicTrigPins[i], OUTPUT);
      pinMode(ultrasonicEchoPins[i], INPUT);
  }  

  ultrasonicTimer[0] = millis() + 2000;           // First ping starts at 2000ms
  for (uint8_t i = 1; i < ULTRASONIC_NUM; i++) { // Set the starting time for each sensor.
      ultrasonicTimer[i] = ultrasonicTimer[i - 1] + ULTRASONIC_INTERVAL;
  }

  // Serial.print("SETUP : ");
  // for(uint8_t i = 0; i < ULTRASONIC_NUM; i++) {
  //   Serial.println(ultrasonicTimer[i]);
  // }


  // SET PINS FOR SERVOMOTOR
  bigAssServo.attach(bigAssServoPin);
  bigAssServo.write(pos);

  

  
  // SET UP MAXON MOTOR CONTROLLER ********************
  pinMode(leftMotorEnablePin, OUTPUT);
  pinMode(leftMotorDirectionPin, OUTPUT);
  pinMode(leftMotorPwmPin, OUTPUT);
  pinMode(rightMotorEnablePin, OUTPUT);
  pinMode(rightMotorDirectionPin, OUTPUT);
  pinMode(rightMotorPwmPin, OUTPUT);

  digitalWrite(leftMotorEnablePin, HIGH); 
  digitalWrite(leftMotorDirectionPin, HIGH); 
  digitalWrite(rightMotorEnablePin, HIGH);
  digitalWrite(rightMotorDirectionPin, HIGH);

  analogWrite(leftMotorPwmPin,26); // pwm duty cycle set initially to 10% (corresponds to 0 speed)
  analogWrite(rightMotorPwmPin,26); // pwm duty cycle set initially to 10% (corresponds to 0 speed)

  // SET UP DC MOTOR DRIVER *********************
  pinMode(MotorADirectionPin, OUTPUT);
  pinMode(MotorAPwmPin, OUTPUT);
  pinMode(MotorAEncoderPin, INPUT);
  pinMode(MotorBDirectionPin, OUTPUT);
  pinMode(MotorBPwmPin, OUTPUT);
  pinMode(MotorBEncoderPin, INPUT);

  digitalWrite(MotorADirectionPin, LOW);
  digitalWrite(MotorBDirectionPin, HIGH);
  analogWrite(MotorAPwmPin, 125);
  analogWrite(MotorBPwmPin, 125);

  Serial.println("Hello from Arduinoooo");
  delay(1000); // wait 2 seconds

}








/********************* LOOP *********************/ 

void loop() {


  for (uint8_t i = 0; i < ULTRASONIC_NUM; i++) { // Loop through all the sensors.
      if (millis() >= ultrasonicTimer[i]) {         // Is it this sensor's time to ping?
          SonarSensor(ultrasonicTrigPins[i], ultrasonicEchoPins[i]);
          cm[i] = distance;
          //Serial.print("Distance with ultrasonic ");
          //Serial.print(i);
          //Serial.print(" at time ");
          //Serial.print(ultrasonicTimer[i]);
          //Serial.print(" : ");
          //Serial.println(distance);

          ultrasonicTimer[i] += ULTRASONIC_INTERVAL * ULTRASONIC_NUM;  // Set next time this sensor will be pinged.
      }      
  }





  readSerialPort();
  updateMotorCommands();

  sensorAvoidance();

  updatePoluluCommands();

  //sendData(); // only send data on request from rasberry pi (go in readSerialPort() function)


  // batteryLevel = analogRead(A15);
  // Serial.print("Battery Level : ");
  // Serial.println(batteryLevel);


  delay(100); // makes things more fluid

}



void updatePoluluCommands() {

  if (polulusOn == 1) {

      Speed = analogRead(A0); // converts analog signal of potentiometer into value between 0 and 1023

      // COMMANDS TO DC MOTOR CONTROLLER
      analogWrite (MotorAPwmPin,map(Speed, 0, 1023, 0, 125));      //PWM Speed Control   
      analogWrite (MotorBPwmPin,map(Speed, 0, 1023, 0, 125)); 

      // MEASURE POLULU MOTOR SPEED
      ontimeA = pulseIn(MotorAEncoderPin,HIGH, 500000);
      ontimeB = pulseIn(MotorBEncoderPin,HIGH, 500000);
      offtimeA = pulseIn(MotorAEncoderPin,LOW, 500000);
      offtimeB = pulseIn(MotorBEncoderPin,LOW, 500000);
      periodA = ontimeA+offtimeA;
      freqA =   1000000.0/periodA;
      //dutyA = (ontimeA/periodA)*100;
      
      //Serial.println(dutyA);
      //Serial.println(periodA);
      periodB = ontimeB+offtimeB;
      freqB =   1000000.0/periodB;
      //dutyB = (ontimeB/periodB)*100;
      Serial.print(freqA);
      Serial.print("  ");
      Serial.println(freqB);
  
      if (freqA < 1500 || freqB < 1500 || freqA > 5000 || freqB > 5000) {
        if (motorAstuck == 0){
          // Serial.println("Motor A stuck");
          digitalWrite(MotorADirectionPin, HIGH);
          digitalWrite(MotorBDirectionPin, LOW);
          motorAstuck = 1;
          delay(100);
        }
      }
      else {
        if (motorAstuck == 1){
          digitalWrite(MotorADirectionPin, LOW);
          digitalWrite(MotorBDirectionPin, HIGH);
          motorAstuck = 0;
          delay(100);
        }
      }
 
  }

  else {
      analogWrite (MotorAPwmPin,0);      // Stop Polulu motors   
      analogWrite (MotorBPwmPin,0);
      delay(100);
  }

}




void SonarSensor(int trigPin,int echoPin)
{
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
  duration = pulseIn(echoPin, HIGH);
  distance = duration/58.2;
}


void sensorAvoidance() {
  // If obstacles detected, override previous motor commands
  if ( (cm[2] <= 20 && cm[2]>0) || (cm[1] <= 20 && cm[1]>0) ){
    digitalWrite(rightMotorDirectionPin, LOW);
  }
  else {
    digitalWrite(rightMotorDirectionPin, HIGH);
  }


  if ( (cm[3] <= 20 && cm[3]>0) || (cm[4] <= 20 && cm[4]>0) ){
    digitalWrite(leftMotorDirectionPin, LOW);
  }
   else {
    digitalWrite(leftMotorDirectionPin, HIGH);
  }
}


void updateMotorCommands() {
  left_speed = forward_speed - angular_speed;
  right_speed = forward_speed + angular_speed;
  // Serial.println(left_speed);
  // Serial.println(right_speed);

  if (left_speed < 0) {
    digitalWrite(leftMotorDirectionPin, LOW);
  }
  else {
    digitalWrite(leftMotorDirectionPin, HIGH);
  }
  if (right_speed < 0) {
    digitalWrite(rightMotorDirectionPin, LOW);
  }
  else {
    digitalWrite(rightMotorDirectionPin, HIGH);
  }
  // NOTE : to communicate with ESCON MAXON cntroller, pwm duty cycle must be between 10% and 90%
  analogWrite(leftMotorPwmPin, map(abs(left_speed), 0, 198, 26, 230));
  analogWrite(rightMotorPwmPin , map(abs(right_speed), 0, 198, 26, 230)); // desired speed sent as pwm to ESCON controller
}

// Source of this : https://www.aranacorp.com/en/serial-communication-between-raspberry-pi-and-arduino/
// Our own protocol:
// send motor commands as for example "m+08-99" (total string lenght of 7)
// send unloading command as "u"
// send emergency STOP command as "s"
// send polulu command as "p0" or "p1"
void readSerialPort() {
  msg = "";
  if (Serial.available()) {
    delay(5);
    msg = Serial.readStringUntil('\n');


    if (msg[0] == 'm') { // m for motor commands
      // Serial.println("MOTOR DATA");
      if (msg.length() == 7){
        String msg_forward_speed = msg.substring(1, 4);
        String msg_angular_speed = msg.substring(4,7);
        forward_speed = msg_forward_speed.toInt();
        angular_speed = msg_angular_speed.toInt();

        // Serial.println(forward_speed);
        // Serial.println(angular_speed);
        digitalWrite(leftMotorEnablePin, HIGH);
        digitalWrite(rightMotorEnablePin, HIGH);
      }
    }
    else if (msg[0]=='u'){ // u for duplo unloading
      Serial.println("UNLOADING");
      digitalWrite(leftMotorEnablePin, HIGH);
      digitalWrite(rightMotorEnablePin, HIGH);
      moveServo();

    }
    else if (msg[0]=='s'){
      Serial.println("STOP NOW!!");
      digitalWrite(leftMotorEnablePin, LOW);
      digitalWrite(rightMotorEnablePin, LOW);
    }
    else if (msg[0]=='p'){
      if (msg == "p0") {
        polulusOn = 0;
      }
      else if (msg == "p1") {
        polulusOn = 1;
      }
    }
    else if (msg[0]=='d'){
      sendData();
    }


    Serial.flush();
  }
}

void sendData() {
  //write data
  Serial.print("d"); // print data
  for(uint8_t i = 0; i < ULTRASONIC_NUM; i++) {
    Serial.print(cm[i]);
    Serial.print(" ");
  }
  Serial.println("");

}


void moveServo() {

  for (pos = 5; pos <= 175; pos += 1) { // goes from 0 degrees to 180 degrees
    // in steps of 1 degree
    bigAssServo.write(pos);              // tell servo to go to position in variable 'pos'
    delay(15);                       // waits 15 ms for the servo to reach the position
  }
  for (pos = 175; pos >= 5; pos -= 1) { // goes from 180 degrees to 0 degrees
    bigAssServo.write(pos);              // tell servo to go to position in variable 'pos'
    delay(15);                       // waits 15 ms for the servo to reach the position
  }
}

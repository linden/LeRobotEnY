// Latest changes : 
// raspberry pi must send request for arduino to send data
// servo motor for unloading
// added customized time delay when reversing direction of polulu motors (works better)
// measure battery level with analogread(A15)
// LEDs to look cool

// NOTE : depending on ULTRASONIC_INTERVAL value, get more or less accurates values from sensors (need to tune at the end)
// NOTE : need to send command to Polulu motors for motors to start turning 


/* ARDUINO CODE 
  by Noah, last updated April 19 2023
  So far implements:
  - Maxon motor speed commands (Using potentiometer on A0)
  - Maxon motor sensor data reading
  - Polulu control and encoder reading (direction reversed if motorA stuck)

  - Serial communication with Raspberry pi
    Motor commands : "m+99+00"
    Unloading command : "u"
    Emergency STOP command : "s"
    Raspberry pi requests data : "d"
        Upon request, data formats received from arduino to raspberry pi :
            "dxxx xxx xxx xxx xxx xxx " with "xxx" representing distance measured by ultrasonic sensor in cm 
        As soon as low battery detected : "b" sent continuously, indicating low battery voltage -> robot should return to collecting point and stop brushes from turning to save battery      
    Polulu motor command : "p0" to turn off, "p1" to turn on (default)



  WARNING only left brosse motor (motorA) detect if duplo gets stuck instead of both motors detecting
    (but turns out works better)

  WARNING ultrasonic sensor sometimes ouptut a value of 0 when not detecting any obstacles
    (may need to implement filtering later)




  MAXON LEFT MOTOR CONNECTED TO MAXON CONTROLLER # 4

  LEFT MOTOR MONITORING : speed A8, current A9

*/ 



#include <Servo.h>
#include <FastLED.h>

#include <Wire.h>
#include <Adafruit_PWMServoDriver.h>

Adafruit_PWMServoDriver pwm = Adafruit_PWMServoDriver();



#define SERVOMIN  200 // This is the 'minimum' pulse length count (out of 4096)
#define SERVOMAX  500 // This is the 'maximum' pulse length count (out of 4096)

#define SERVO_FREQ 60 // Analog servos run at ~50 Hz updates

#define LED_PIN     7
#define NUM_LEDS    32

#define ULTRASONIC_INTERVAL 40 // Milliseconds between sensor pings (29ms is about the min to avoid cross-sensor echo).
#define ULTRASONIC_NUM 5 // Number of sensors




bool DEBUG = 1;
bool use_adafruit_driver = 1;


// FSM state
int fsmState = 0; // when state = 0, simple forward movement and obstacle avoidance

// Polulu motor state and default speed
bool polulusOn = 1;
int poluluSpeed = 70; 

bool obstacleOverride = 0;

uint8_t servonum = 0;


// LEDs
CRGB leds[NUM_LEDS];
// bool batteryLedsState[4] = {1,1,1,1};
// bool prevBatteryLedsState[4] = {1,1,1,1};
int ledStates[NUM_LEDS] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
int prevLedStates[NUM_LEDS] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};


unsigned long currentTime = 0;

unsigned long ultrasonicTimer[ULTRASONIC_NUM]; // Holds the times when the next ping should happen for each sensor.
unsigned int cm[ULTRASONIC_NUM] = {100,100,100,100,100};         // Where the measured ultrasonic distances are stored.
bool obstacleState[ULTRASONIC_NUM] = {0,0,0,0,0};
//uint8_t currentUltrasonicSensor = 0;          // Keeps track of which sensor is active.
//unsigned int  ultrasonicSensors[ULTRASONIC_NUM];
// ULTRASONIC PINS
// const int ultrasonicTrigPins[ULTRASONIC_NUM] = {30,32,34,36,38,40};
// const int ultrasonicEchoPins[ULTRASONIC_NUM] = {31,33,35,37,39,41};
const int ultrasonicTrigPins[ULTRASONIC_NUM] = {42,32,34,36,38};
const int ultrasonicEchoPins[ULTRASONIC_NUM] = {43,33,35,37,39};



// SERVOMOTOR
const int bigAssServoPin = 3;
int pos = 30;
Servo bigAssServo;





// MAXON CONTROLLER PINS
const int leftMotorEnablePin = 22;
const int leftMotorDirectionPin = 23;
const int leftMotorPwmPin = 10;

const int rightMotorEnablePin = 24;
const int rightMotorDirectionPin = 25;
const int rightMotorPwmPin = 11;

// DC MOTOR DRIVER PINS
const int MotorADirectionPin = 52;
const int MotorAPwmPin = 8;
const int MotorAEncoderPin = 50;

const int MotorBDirectionPin = 53;
const int MotorBPwmPin = 9;
const int MotorBEncoderPin = 51;


unsigned long startMillis;  // some global variables available anywhere in the program
unsigned long currentMillis;
const unsigned long poluluPeriod = 1000;  // the value is a number of milliseconds
const unsigned long poluluPeriodUnstuck = 200;


unsigned long fsmStartMillis;
unsigned long fsmCurrentMillis;
const unsigned long fsmPeriod = 1000;
unsigned long obstacleCounter = 0;

unsigned long serialStartMillis;
unsigned long serialCurrentMillis;




int speedLeft = 0;
int currentLeft = 0;
int speedRight = 0;
int currentRight = 0;
// Battery Monitoring
int batteryLevel = 0;



int ontimeA,offtimeA,dutyA;
float freqA, periodA;
int ontimeB,offtimeB,dutyB;
float freqB, periodB;

// Variables for ultrasonic sensors
long duration, distance, sonicSensor1;

// Communication with Raspberry Pi : 
String msg = "";

int left_speed = 99; // WARNING value between -198 and 198
int right_speed = 99; // WARNING value between -198 and 198

// BLABLA
bool motorAstuck = 0;
bool motorBstuck = 0;

// FUNCTIONS
void SonarSensor();
void updateMotorCommands();
void sendData();
void readSerialPort();
void sensorAvoidance();
void updatePoluluCommands();
void unloading();
void moveServos();
void checkBatteryLevel();

void checkMotors();
void fsm();
void newFSMstate(int stateNumber = 0, int stateDuration = 1000);

void updateLEDs();

/********************* SETUP *********************/

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200); // Initialize serial communication with computer

  // while (!Serial) {
  //   delay(10); // wait for serial port to connect. Needed for native USB port only
  // }


  // LEDs Initialization
  FastLED.addLeds<WS2812, LED_PIN, GRB>(leds, NUM_LEDS);
  // FastLED.setBrightness(10);
  for (uint8_t i = 0; i <= 31; i++) {
    leds[i] = CRGB (0, 0, 10); // blue
    FastLED.show();
    delay(40);
  }
  for (uint8_t i = 0; i <= 31; i++) {
    leds[i] = CRGB (0, 0, 0); // blue
    FastLED.show();
    delay(40);
  }

  if (use_adafruit_driver) {
      pwm.begin();
      pwm.setOscillatorFrequency(27000000);
      pwm.setPWMFreq(SERVO_FREQ);  // Analog servos run at ~50 Hz updates
      pwm.setPWM(servonum, 0, SERVOMIN);
      delay(1000);
      pwm.setPWM(servonum, 0, SERVOMAX);
      delay(1000);
      pwm.setPWM(servonum, 0, SERVOMIN);
      delay(1000);
      pwm.setPWM(0, 0, 0);  // turn off PWM signal to channel 0
  }
  else {
    // SET PINS FOR SERVOMOTOR
    bigAssServo.attach(bigAssServoPin);
    bigAssServo.write(30);
    delay(1000);
    bigAssServo.write(130);
    delay(1000);
    bigAssServo.write(30);
    delay(1000);
  }







  // SET PINMODE OF ULTRASONIC SENSORS
  for (uint8_t i = 0; i < ULTRASONIC_NUM; i++) { // Set the starting time for each sensor.
      pinMode(ultrasonicTrigPins[i], OUTPUT);
      pinMode(ultrasonicEchoPins[i], INPUT);
  }  

  ultrasonicTimer[0] = millis() + 2000;           // First ping starts at 2000ms
  for (uint8_t i = 1; i < ULTRASONIC_NUM; i++) { // Set the starting time for each sensor.
      ultrasonicTimer[i] = ultrasonicTimer[i - 1] + ULTRASONIC_INTERVAL;
  }

  // Serial.print("SETUP : ");
  // for(uint8_t i = 0; i < ULTRASONIC_NUM; i++) {
  //   Serial.println(ultrasonicTimer[i]);
  // }




  

  
  // SET UP MAXON MOTOR CONTROLLER ********************
  pinMode(leftMotorEnablePin, OUTPUT);
  pinMode(leftMotorDirectionPin, OUTPUT);
  pinMode(leftMotorPwmPin, OUTPUT);
  pinMode(rightMotorEnablePin, OUTPUT);
  pinMode(rightMotorDirectionPin, OUTPUT);
  pinMode(rightMotorPwmPin, OUTPUT);

  digitalWrite(leftMotorEnablePin, HIGH); 
  digitalWrite(leftMotorDirectionPin, HIGH); 
  digitalWrite(rightMotorEnablePin, HIGH);
  digitalWrite(rightMotorDirectionPin, HIGH);

  analogWrite(leftMotorPwmPin,26); // pwm duty cycle set initially to 10% (corresponds to 0 speed)
  analogWrite(rightMotorPwmPin,26); // pwm duty cycle set initially to 10% (corresponds to 0 speed)

  // SET UP DC MOTOR DRIVER *********************
  pinMode(MotorADirectionPin, OUTPUT);
  pinMode(MotorAPwmPin, OUTPUT);
  pinMode(MotorAEncoderPin, INPUT);
  pinMode(MotorBDirectionPin, OUTPUT);
  pinMode(MotorBPwmPin, OUTPUT);
  pinMode(MotorBEncoderPin, INPUT);

  digitalWrite(MotorADirectionPin, LOW);
  digitalWrite(MotorBDirectionPin, HIGH);
  analogWrite(MotorAPwmPin, 80);
  analogWrite(MotorBPwmPin, 80);


  // Battery level indication :
  // leds[14] = CRGB ( 0, 10, 0); // green
  // leds[15] = CRGB ( 0, 10, 0); // green
  // leds[16] = CRGB ( 0, 10, 0); // green
  // leds[17] = CRGB ( 0, 10, 0); // green
  checkBatteryLevel();
  // FastLED.show();
  updateLEDs();


  // For polulus
  startMillis = millis();
  currentMillis = startMillis;

  fsmStartMillis = millis();
  fsmCurrentMillis = millis();

  serialStartMillis = millis();
  serialCurrentMillis = serialStartMillis;


  // Say hello for sake of politeness
  // Serial.println("Hello from Arduinoooo");
  delay(1000); // wait 1 second


}



/********************* LOOP *********************/ 

void loop() {


  for (uint8_t i = 0; i < ULTRASONIC_NUM; i++) { // Loop through all the sensors.
      if (millis() >= ultrasonicTimer[i]) {         // Is it this sensor's time to ping?
          SonarSensor(ultrasonicTrigPins[i], ultrasonicEchoPins[i]);
          cm[i] = distance;
          ultrasonicTimer[i] += ULTRASONIC_INTERVAL * ULTRASONIC_NUM;  // Set next time this sensor will be pinged.
      }      
  }

  sendData();

  
  // WARNING set polulus on by default
  // polulu may be turned off if : 
  // 1) ordered off by readSerialPort()  
  // 2) ordered off by checkMotors() (Maxon motors stalling) 
  // 3) ordered off by checkBatteryLevel() (battery level too low)

  polulusOn = 1;  

  serialStartMillis = millis();
  readSerialPort(); // this function may set polulus to 0
  Serial.print("Serial Port time took : ");
  Serial.println(millis() - serialStartMillis);  

  checkMotors(); // this function may set polulus to 0
  
  checkBatteryLevel(); // this function may set polulus to 0

  updatePoluluCommands(); 

  fsm();

  updateLEDs();


}




/********************* FUNCTIONS *********************/



void fsm() {

  switch (fsmState) {
      case 0:
        updateMotorCommands();
        sensorAvoidance();
        break;
      case 1: // LEFT TURN OF DURATION 
        fsmCurrentMillis = millis();
        digitalWrite(leftMotorDirectionPin, LOW);
        digitalWrite(rightMotorDirectionPin, HIGH);
        analogWrite(leftMotorPwmPin, map(99, 0, 99, 26, 230));
        analogWrite(rightMotorPwmPin, map(99, 0, 99, 26, 230));
        // time spent turning should be proportional to wheel speed
        if (fsmCurrentMillis - fsmStartMillis >= 1500) {
          // fsmState = 0;
          newFSMstate(0, 1000);
        }
        break;

      case 2: // LEFT TURN OF DURATION 
        fsmCurrentMillis = millis();
        digitalWrite(leftMotorDirectionPin, HIGH);
        digitalWrite(rightMotorDirectionPin, LOW);
        analogWrite(leftMotorPwmPin, map(99, 0, 99, 26, 230));
        analogWrite(rightMotorPwmPin, map(99, 0, 99, 26, 230));

        // time spent turning should be proportional to wheel speed
        if (fsmCurrentMillis - fsmStartMillis >= 1500) {
          // fsmState = 0;
          newFSMstate(0, 1000);
        }
        break;
  
      case 3: // BACKWARDS
        fsmCurrentMillis = millis();
        digitalWrite(leftMotorDirectionPin, LOW);
        digitalWrite(rightMotorDirectionPin, LOW);
        analogWrite(leftMotorPwmPin, map(99, 0, 99, 26, 230));
        analogWrite(rightMotorPwmPin, map(99, 0, 99, 26, 230));
        if (fsmCurrentMillis - fsmStartMillis >= 2000) {
          // fsmStartMillis = millis();
          // fsmState = 1;
          newFSMstate(1, 1000);
        }
        break;
      case 4: // FORWARDS
        fsmCurrentMillis = millis();
        digitalWrite(leftMotorDirectionPin, HIGH);
        digitalWrite(rightMotorDirectionPin, HIGH);
        analogWrite(leftMotorPwmPin, map(99, 0, 99, 26, 230));
        analogWrite(rightMotorPwmPin, map(99, 0, 99, 26, 230));

        if (fsmCurrentMillis - fsmStartMillis >= 2000) {
          // fsmStartMillis = millis();
          // fsmState = 1;
          newFSMstate(1, 1000);
        }
        break;
      default:
        fsmState = 0;
        break;
    }

}

void updatePoluluCommands() {

  if (polulusOn == 1 && !DEBUG) {

      // COMMANDS TO DC MOTOR CONTROLLER
      analogWrite (MotorAPwmPin, poluluSpeed);      //PWM Speed Control   
      analogWrite (MotorBPwmPin, poluluSpeed); 

      // MEASURE POLULU MOTOR SPEED
      ontimeA = pulseIn(MotorAEncoderPin,HIGH, 500000);
      ontimeB = pulseIn(MotorBEncoderPin,HIGH, 500000);
      offtimeA = pulseIn(MotorAEncoderPin,LOW, 500000);
      offtimeB = pulseIn(MotorBEncoderPin,LOW, 500000);
      periodA = ontimeA+offtimeA;
      freqA =   1000000.0/periodA;

      periodB = ontimeB+offtimeB;
      freqB =   1000000.0/periodB;

      // Serial.print("FreqA : ");
      // Serial.print(freqA);
      // Serial.print(" FreqB : ");
      // Serial.println(freqB);


      // Note : if value is inf (meaning period is 0) then motor powered off or not connected to power source

      if (motorAstuck == 0){
        currentMillis = millis();  //get the current time 
        if (currentMillis - startMillis >= poluluPeriodUnstuck) {
          if (freqA < 200 || freqB < 200 || freqA > 5000 || freqB > 5000) {
            digitalWrite(MotorADirectionPin, HIGH);
            digitalWrite(MotorBDirectionPin, LOW);
            motorAstuck = 1;
            startMillis = millis();  //initial start time when motors considered stuck
          }          
        }
      }

      if (motorAstuck == 1){
        currentMillis = millis();  //get the current time 
        if (currentMillis - startMillis >= poluluPeriod) {
            digitalWrite(MotorADirectionPin, LOW);
            digitalWrite(MotorBDirectionPin, HIGH);
            motorAstuck = 0;
            // delay(100); // avoid delays using millis() below :
            startMillis = millis();  //initial start time when motors considered unstuck
        }
      }
  }

  else {
      analogWrite (MotorAPwmPin,0);      // if polulusOn == 0, Stop Polulu motors   
      analogWrite (MotorBPwmPin,0);
  }  

}

void SonarSensor(int trigPin,int echoPin) {

  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
  duration = pulseIn(echoPin, HIGH);
  distance = duration/58.2;
}

void sensorAvoidance() {
  // If obstacles detected, override previous motor commands
  bool obstacleLeft = 0;
  bool obstacleRight = 0;
  int threshold = 20;

  // if (obstacleOverride) {
  //   // "Override" obstacle avoidance (use smaller margin for distance sensors)
  //   threshold = 5;
  // }
  // else {
  //   threshold = 20;
  // }

  if ( (cm[2] <= threshold && cm[2]>0) || (cm[1] <= threshold && cm[1]>0) ){
    obstacleLeft = 1;
    digitalWrite(rightMotorDirectionPin, LOW);
    // analogWrite(rightMotorPwmPin, map(abs(right_speed-40), 0, 198, 26, 230)); // WARNING useful???
    analogWrite(rightMotorPwmPin, map(99, 0, 99, 26, 230));
    for (int i = 22; i <= 31; i++) {
      if (i%2 == 1) {
        ledStates[i] = 4; // purple
      }
    }

  }
  else {
    obstacleLeft = 0;
    digitalWrite(rightMotorDirectionPin, HIGH);
    for (int i = 22; i <= 31; i++) {
      if (i%2 == 1) {
        ledStates[i] = 0; // off
      }
    }
  }


  if ( (cm[3] <= threshold && cm[3]>0) || (cm[4] <= threshold && cm[4]>0) ){
    obstacleRight = 1;
    digitalWrite(leftMotorDirectionPin, LOW);
    // analogWrite(leftMotorPwmPin, map(abs(left_speed-40), 0, 198, 26, 230)); // WARNING useful???
    analogWrite(leftMotorPwmPin, map(99, 0, 99, 26, 230));
    for (int i = 0; i <= 9; i++) {
      if (i%2 == 1) {
        ledStates[i] = 4; // purple
      }
    }
  }
  else {
    obstacleRight = 0;
    digitalWrite(leftMotorDirectionPin, HIGH);
    for (int i = 0; i <= 9; i++) {
      if (i%2 == 1) {
        ledStates[i] = 0; // off
      }
    }
  }

  // if (obstacleLeft || obstacleRight) { 
  //   obstacleCounter++;
  //   // Serial.println(obstacleCounter);
  //   if (obstacleCounter >= 150) {
  //     fsmStartMillis = millis();
  //     obstacleCounter = 0;
  //     fsmState = 1; // go left or right      
  //   }
  // }

  if (cm[0] <= 7 && cm[0]>0) {
    newFSMstate(3, 1000); // go backwards
    ledStates[13] = 4;
    ledStates[18] = 4;
  }
  else {
    ledStates[13] = 0;
    ledStates[18] = 0;
  }
}

void updateMotorCommands() {

  if (left_speed < 0) {
    digitalWrite(leftMotorDirectionPin, LOW);
  }
  else {
    digitalWrite(leftMotorDirectionPin, HIGH);
  }
  if (right_speed < 0) {
    digitalWrite(rightMotorDirectionPin, LOW);
  }
  else {
    digitalWrite(rightMotorDirectionPin, HIGH);
  }
  // NOTE : to communicate with ESCON MAXON cntroller, pwm duty cycle must be between 10% and 90%
  analogWrite(leftMotorPwmPin, map(abs(left_speed), 0, 99, 26, 230));
  analogWrite(rightMotorPwmPin , map(abs(right_speed), 0, 99, 26, 230)); // desired speed sent as pwm to ESCON controller
}

void readSerialPort() {

  // Code taken from : https://www.aranacorp.com/en/serial-communication-between-raspberry-pi-and-arduino/
  // Our own protocol:
  // send motor commands as for example "m+08-99" (total string lenght of 7)
  // send unloading command as "u"
  // send emergency STOP command as "s"
  // send polulu command as "p0" or "p1"
  
  msg = "";
  while (Serial.available()) {
    msg = Serial.readStringUntil('\n');
    // Serial.print("In while loop : ");
    // Serial.println(msg);
  }
  if (msg != "") {
    // Serial.print("Afgter while loop : ");
    // Serial.println(msg);
    // delay(5);
    // msg = Serial.readStringUntil('\n');


    if (msg[0] == 'm') { // m for motor commands
      // Serial.println("MOTOR DATA");
      if (msg.length() == 7){
        String msg_forward_speed = msg.substring(1, 4);
        String msg_angular_speed = msg.substring(4,7);
        left_speed = msg_forward_speed.toInt();
        right_speed = msg_angular_speed.toInt();

        // Serial.print("Arduino read : ");
        // Serial.println(msg);
        // Serial.println(angular_speed);
        digitalWrite(leftMotorEnablePin, HIGH);
        digitalWrite(rightMotorEnablePin, HIGH);
      }
    }
    else if (msg[0]=='a') {
      // ARDUINO IN CHARGE OF EVERYTHING      
      left_speed = 99;
      right_speed = 99;
      digitalWrite(leftMotorEnablePin, HIGH);
      digitalWrite(rightMotorEnablePin, HIGH);
    }
    else if (msg[0]=='u'){ // u for duplo unloading
      // Serial.println("UNLOADING");
      unloading();
    }
    else if (msg[0]=='s'){
      // Serial.println("STOP NOW!!");
      digitalWrite(leftMotorEnablePin, LOW);
      digitalWrite(rightMotorEnablePin, LOW);
    }
    else if (msg[0]=='r'){
      // resume (start)
      digitalWrite(leftMotorEnablePin, HIGH);
      digitalWrite(rightMotorEnablePin, HIGH);
    }
    // else if (msg[0]=='p'){
    //   if (msg == "p0") {
    //     polulusOn = 0;
    //   }
    //   else if (msg == "p1") {
    //     polulusOn = 1;
    //   }
    // }
    else if (msg[0]=='D'){
      if (msg == "D0") {
        DEBUG = 0;
      }
      else if (msg == "D1") {
        DEBUG = 1;
      }
    }
    else if (msg[0]=='d'){
      sendData();
    }
    // Serial.flush();
  }
  // WARNING do this
  // else {
  //   // count amount of times it did not receive a message to determine if Arduino should take over
  //   left_speed = 99;
  //   right_speed = 99;
  //   digitalWrite(leftMotorEnablePin, HIGH);
  //   digitalWrite(rightMotorEnablePin, HIGH);
  // }
}

void sendData() {
  //write data
  Serial.print("d"); // print data
  for(uint8_t i = 0; i < ULTRASONIC_NUM; i++) {
    Serial.print(cm[i]);
    Serial.print(" ");
  }
  Serial.println(""); // return to line indicates end of message

}

void checkBatteryLevel() {

  // polulusOn = 1;

  // Battery Level Monitoring
  // 100% : >= 830, 75% : >= 798, 50% : >= 757, 25% : >= 716
  batteryLevel = analogRead(A15);
  // Serial.print("BATTERY LEVEL : ");
  // Serial.println(batteryLevel);
  if (batteryLevel >= 815) {

    ledStates[14] = 2;
    ledStates[15] = 2;
    ledStates[16] = 2;
    ledStates[17] = 1;
  }

  if (batteryLevel < 815 && batteryLevel >= 788) {

    ledStates[14] = 2;
    ledStates[15] = 2;
    ledStates[16] = 2;
    ledStates[17] = 1;
  }
  else if (batteryLevel < 788 && batteryLevel >= 751) {

    ledStates[14] = 2;
    ledStates[15] = 2;
    ledStates[16] = 1;
    ledStates[17] = 1;
  }
  else if (batteryLevel < 751 and batteryLevel >= 715) {

    ledStates[14] = 2;
    ledStates[15] = 1;
    ledStates[16] = 1;
    ledStates[17] = 1;
  }

  else if (batteryLevel < 715) {
    // NEED TO WARN RASPBERRY PI
    // Serial.println("b");
    polulusOn = 0;
 
    ledStates[14] = 1;
    ledStates[15] = 1;
    ledStates[16] = 1;
    ledStates[17] = 1;
  }
}

void checkMotors() {
  speedLeft = analogRead(A9);
  currentLeft = analogRead(A8);
  speedRight = analogRead(A4);
  currentRight = analogRead(A5);

  if (currentLeft >= 550 || currentRight >= 550) {
    // motors stuck going forwards
    // turn off polulu motors to avoid too much current draw
    polulusOn = 0;

    // fsmStartMillis = millis();
    // fsmState = 2; 
    newFSMstate(3, 2000);   
  }
  else if ( (currentLeft <= 270 && currentLeft >= 10) || (currentRight <= 270 && currentRight >= 10) ) {
    // motors stuck going backwards
    // added >= 10 to avoid polulus turning off when no proper analog input detected
    // turn off polulu motors to avoid too much current draw
    
    polulusOn = 0;
    newFSMstate(0, 1000);
  }
  // else {
  //   polulusOn = 1;
  // }
}



// int myMultiplyFunction(int x, int y){
// 	  int result;
// 	  result = x * y;
// 	  return result;
// }

void newFSMstate(int stateNumber = 0, int stateDuration = 1000) {
  fsmStartMillis = millis();
  fsmState = stateNumber;
}

void updateLEDs() {
  // ONLY UPDATES LEDS WHEN CHANGE OF STATE
  for (uint8_t i = 0;  i < NUM_LEDS; i++) {
    if (prevLedStates[i] != ledStates[i]) {
      switch (ledStates[i]) {
        case 0:
          leds[i] = CRGB ( 0, 0, 0); // off
          break;
        case 1:
          leds[i] = CRGB ( 10, 0, 0); // red
          break;
        case 2:
          leds[i] = CRGB ( 0, 10, 0); // green
          break;
        case 3:
          leds[i] = CRGB ( 0, 0, 10); // blue
          break;
        case 4: 
          leds[i] = CRGB ( 10, 0, 10); // pink
          break;
        default:
          leds[i] = CRGB ( 0, 0, 0); // off
          break;
      }

      FastLED.show(); 
      prevLedStates[i] = ledStates[i];     
    }
  }
}

void moveServos() {

  if (use_adafruit_driver) {

  for (uint16_t pulselen = SERVOMIN; pulselen < SERVOMAX; pulselen++) {
    pwm.setPWM(servonum, 0, pulselen);
    if (pulselen%100 == 0) {
      int pos_divided_by_100 = pulselen/100;
      if (pos_divided_by_100%2 == 0) {
        digitalWrite(leftMotorDirectionPin, HIGH);
        digitalWrite(rightMotorDirectionPin, LOW);
      }
      else {
        digitalWrite(leftMotorDirectionPin, LOW);
        digitalWrite(rightMotorDirectionPin, HIGH);
      }
    }
    delay(3);
  }
  delay(3);
  digitalWrite(leftMotorDirectionPin, HIGH);
  digitalWrite(rightMotorDirectionPin, HIGH);

  for (uint16_t pulselen = SERVOMAX; pulselen > SERVOMIN; pulselen--) {
    pwm.setPWM(servonum, 0, pulselen);
    if (pulselen%100 == 0) {
      int pos_divided_by_100 = pulselen/100;
      if (pos_divided_by_100%2 == 0) {
        digitalWrite(leftMotorDirectionPin, HIGH);
        digitalWrite(rightMotorDirectionPin, LOW);
      }
      else {
        digitalWrite(leftMotorDirectionPin, LOW);
        digitalWrite(rightMotorDirectionPin, HIGH);
      }
    }
    delay(3);
  }
  delay(3);
  digitalWrite(leftMotorDirectionPin, HIGH);
  digitalWrite(rightMotorDirectionPin, HIGH);

  }



  else {

  for (pos = 30; pos <= 165; pos += 1) { // goes from 0 degrees to 180 degrees
    // in steps of 1 degree
    bigAssServo.write(pos);              // tell servo to go to position in variable 'pos'
    if (pos%30 == 0) {
      int pos_divided_by_30 = pos/30;
      if (pos_divided_by_30%2 == 0) {
        digitalWrite(leftMotorDirectionPin, HIGH);
        digitalWrite(rightMotorDirectionPin, LOW);
      }
      else {
        digitalWrite(leftMotorDirectionPin, LOW);
        digitalWrite(rightMotorDirectionPin, HIGH);
      }
    }
    delay(15);                       // waits 15 ms for the servo to reach the position
  }

  digitalWrite(leftMotorDirectionPin, HIGH);
  digitalWrite(rightMotorDirectionPin, HIGH);

  for (pos = 165; pos >= 30; pos -= 1) { // goes from 180 degrees to 0 degrees
    bigAssServo.write(pos);              // tell servo to go to position in variable 'pos'
    if (pos%30 == 0) {
      int pos_divided_by_30 = pos/30;
      if (pos_divided_by_30%2 == 0) {
        digitalWrite(rightMotorDirectionPin, HIGH);
        digitalWrite(leftMotorDirectionPin, LOW);
      }
      else {
        digitalWrite(rightMotorDirectionPin, LOW);
        digitalWrite(leftMotorDirectionPin, HIGH);
      }

    }
    delay(15);                       // waits 15 ms for the servo to reach the position
  }
  digitalWrite(leftMotorDirectionPin, HIGH);
  digitalWrite(rightMotorDirectionPin, HIGH);

  }


}

void unloading() {
      digitalWrite(leftMotorEnablePin, HIGH);
      digitalWrite(rightMotorEnablePin, HIGH);
      
      digitalWrite(MotorADirectionPin, HIGH);
      digitalWrite(MotorBDirectionPin, LOW);
      moveServos();
      delay(100);
      moveServos();
      pwm.setPWM(0, 0, 0);  // turn off PWM signal to channel 0
      digitalWrite(MotorADirectionPin, LOW);
      digitalWrite(MotorBDirectionPin, HIGH);
      digitalWrite(leftMotorDirectionPin, HIGH);
      digitalWrite(rightMotorDirectionPin, HIGH);
}






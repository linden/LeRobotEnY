// Latest changes : 
// raspberry pi must send request for arduino to send data
// servo motor for unloading
// added customized time delay when reversing direction of polulu motors (works better)
// measure battery level with analogread(A15)
// LEDs to look cool

// NOTE : depending on ULTRASONIC_INTERVAL value, get more or less accurates values from sensors (need to tune at the end)
// NOTE : need to send command to Polulu motors for motors to start turning 


/* ARDUINO CODE 
  by Noah, last updated April 19 2023
  So far implements:
  - Maxon motor speed commands (Using potentiometer on A0)
  - Maxon motor sensor data reading
  - Polulu control and encoder reading (direction reversed if motorA stuck)

  - Serial communication with Raspberry pi
    Motor commands : "m+99+00"
    Unloading command : "u"
    Emergency STOP command : "s"
    Raspberry pi requests data : "d"
        Upon request, data formats received from arduino to raspberry pi :
            "dxxx xxx xxx xxx xxx xxx " with "xxx" representing distance measured by ultrasonic sensor in cm 
        As soon as low battery detected : "b" sent continuously, indicating low battery voltage -> robot should return to collecting point and stop brushes from turning to save battery      
    Polulu motor command : "p0" to turn off, "p1" to turn on (default)



  WARNING only left brosse motor (motorA) detect if duplo gets stuck instead of both motors detecting
    (but turns out works better)

  WARNING ultrasonic sensor sometimes ouptut a value of 0 when not detecting any obstacles
    (may need to implement filtering later)




  MAXON LEFT MOTOR CONNECTED TO MAXON CONTROLLER # 4

  LEFT MOTOR MONITORING : speed A8, current A9

*/ 



#include <Servo.h>
#include <FastLED.h>

#define LED_PIN     7
#define NUM_LEDS    32

#define ULTRASONIC_INTERVAL 40 // Milliseconds between sensor pings (29ms is about the min to avoid cross-sensor echo).
#define ULTRASONIC_NUM 6 // Number of sensors



// LEDs
CRGB leds[NUM_LEDS];
bool batteryLedsState[4] = {1,1,1,1};
bool prevBatteryLedsState[4] = {1,1,1,1};


unsigned long currentTime = 0;

unsigned long ultrasonicTimer[ULTRASONIC_NUM]; // Holds the times when the next ping should happen for each sensor.
unsigned int cm[ULTRASONIC_NUM] = {100,100,100,100,100,100};         // Where the measured ultrasonic distances are stored.
bool obstacleState[ULTRASONIC_NUM] = {0,0,0,0,0,0};
//uint8_t currentUltrasonicSensor = 0;          // Keeps track of which sensor is active.
//unsigned int  ultrasonicSensors[ULTRASONIC_NUM];
// ULTRASONIC PINS
const int ultrasonicTrigPins[ULTRASONIC_NUM] = {30,32,34,36,38,40};
const int ultrasonicEchoPins[ULTRASONIC_NUM] = {31,33,35,37,39,41};



// SERVOMOTOR
Servo bigAssServo;
const int bigAssServoPin = 3;
int pos = 5;


// MAXON CONTROLLER PINS
const int leftMotorEnablePin = 22;
const int leftMotorDirectionPin = 23;
const int leftMotorPwmPin = 10;

const int rightMotorEnablePin = 24;
const int rightMotorDirectionPin = 25;
const int rightMotorPwmPin = 11;

// DC MOTOR DRIVER PINS
const int MotorADirectionPin = 52;
const int MotorAPwmPin = 8;
const int MotorAEncoderPin = 50;

const int MotorBDirectionPin = 53;
const int MotorBPwmPin = 9;
const int MotorBEncoderPin = 51;

bool polulusOn = 0;
unsigned long startMillis;  // some global variables available anywhere in the program
unsigned long currentMillis;
const unsigned long poluluPeriod = 1000;  // the value is a number of milliseconds
const unsigned long poluluPeriodUnstuck = 200;


unsigned long obstacleStartMillis;
unsigned long obstacleCurrentMillis;
const unsigned long obstaclePeriod = 2000;
unsigned long obstacleCounter = 0;





// VARIABLES FOR ANALOG READ
// Speed read by potentiometer *************************** REMOVE LATER
int Speed; 
int speedLeft = 0;
int currentLeft = 0;
// Battery Monitoring
int batteryLevel = 0;

// Variables for brush dc motors
int SpeedPolulu;
int current_sense_counter = 0;

int ontimeA,offtimeA,dutyA;
float freqA, periodA;
int ontimeB,offtimeB,dutyB;
float freqB, periodB;

// Variables for ultrasonic sensors
long duration, distance, sonicSensor1;

// Communication with Raspberry Pi : 
String msg = "";
int forward_speed =198; // WARNING value between -99 and 99
int angular_speed = 0; // WARNING value between -99 and 99

int left_speed = 0; // WARNING value between -198 and 198
int right_speed = 0; // WARNING value between -198 and 198

// BLABLA
bool motorAstuck = 0;
bool motorBstuck = 0;

// FUNCTIONS
void SonarSensor();
void updateMotorCommands();
void sendData();
void readSerialPort();
void sensorAvoidance();
void updatePoluluCommands();
void moveServo();
void checkBatteryLevel();

void checkMotors();

/********************* SETUP *********************/

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600); // Initialize serial communication with computer


  // LEDs Initialization
  FastLED.addLeds<WS2812, LED_PIN, GRB>(leds, NUM_LEDS);
  for (uint8_t i = 0; i <= 31; i++) {
    leds[i] = CRGB (0, 0, 10); // blue
    FastLED.show();
    delay(40);
  }




  // SET PINMODE OF ULTRASONIC SENSORS
  for (uint8_t i = 0; i < ULTRASONIC_NUM; i++) { // Set the starting time for each sensor.
      pinMode(ultrasonicTrigPins[i], OUTPUT);
      pinMode(ultrasonicEchoPins[i], INPUT);
  }  

  ultrasonicTimer[0] = millis() + 2000;           // First ping starts at 2000ms
  for (uint8_t i = 1; i < ULTRASONIC_NUM; i++) { // Set the starting time for each sensor.
      ultrasonicTimer[i] = ultrasonicTimer[i - 1] + ULTRASONIC_INTERVAL;
  }

  // Serial.print("SETUP : ");
  // for(uint8_t i = 0; i < ULTRASONIC_NUM; i++) {
  //   Serial.println(ultrasonicTimer[i]);
  // }


  // SET PINS FOR SERVOMOTOR
  bigAssServo.attach(bigAssServoPin);
  bigAssServo.write(30);
  delay(100);

  

  
  // SET UP MAXON MOTOR CONTROLLER ********************
  pinMode(leftMotorEnablePin, OUTPUT);
  pinMode(leftMotorDirectionPin, OUTPUT);
  pinMode(leftMotorPwmPin, OUTPUT);
  pinMode(rightMotorEnablePin, OUTPUT);
  pinMode(rightMotorDirectionPin, OUTPUT);
  pinMode(rightMotorPwmPin, OUTPUT);

  digitalWrite(leftMotorEnablePin, HIGH); 
  digitalWrite(leftMotorDirectionPin, HIGH); 
  digitalWrite(rightMotorEnablePin, HIGH);
  digitalWrite(rightMotorDirectionPin, HIGH);

  analogWrite(leftMotorPwmPin,25); // pwm duty cycle set initially to 10% (corresponds to 0 speed)
  analogWrite(rightMotorPwmPin,25); // pwm duty cycle set initially to 10% (corresponds to 0 speed)

  // SET UP DC MOTOR DRIVER *********************
  pinMode(MotorADirectionPin, OUTPUT);
  pinMode(MotorAPwmPin, OUTPUT);
  pinMode(MotorAEncoderPin, INPUT);
  pinMode(MotorBDirectionPin, OUTPUT);
  pinMode(MotorBPwmPin, OUTPUT);
  pinMode(MotorBEncoderPin, INPUT);

  digitalWrite(MotorADirectionPin, LOW);
  digitalWrite(MotorBDirectionPin, HIGH);
  analogWrite(MotorAPwmPin, 125);
  analogWrite(MotorBPwmPin, 125);


  // For polulus
  startMillis = millis();
  currentMillis = startMillis;


  // Say hello for sake of politeness
  Serial.println("Hello from Arduinoooo");
  delay(1000); // wait 2 seconds

  //  for (int i = 0; i <= 31; i++) {
  //   leds[i] = CRGB ( 0, 0, 0); // off
  // }
  // FastLED.show();
  // delay(40);

  // Battery level indication :
  leds[14] = CRGB ( 0, 10, 0); // green
  leds[15] = CRGB ( 0, 10, 0); // green
  leds[16] = CRGB ( 0, 10, 0); // green
  leds[17] = CRGB ( 0, 10, 0); // green
  checkBatteryLevel();
  FastLED.show();


}



/********************* LOOP *********************/ 

void loop() {


  for (uint8_t i = 0; i < ULTRASONIC_NUM; i++) { // Loop through all the sensors.
      if (millis() >= ultrasonicTimer[i]) {         // Is it this sensor's time to ping?
          SonarSensor(ultrasonicTrigPins[i], ultrasonicEchoPins[i]);
          cm[i] = distance;
          ultrasonicTimer[i] += ULTRASONIC_INTERVAL * ULTRASONIC_NUM;  // Set next time this sensor will be pinged.
      }      
  }




  //sendData();

  readSerialPort();

  updateMotorCommands();

  sensorAvoidance();

  updatePoluluCommands();

  checkBatteryLevel();

  FastLED.show();

  checkMotors();

  delay(10); // makes things more fluid

}




/********************* FUNCTIONS *********************/

void updatePoluluCommands() {

  if (polulusOn == 1) {

      Speed = analogRead(A0); // converts analog signal of potentiometer into value between 0 and 1023

      // COMMANDS TO DC MOTOR CONTROLLER
      analogWrite (MotorAPwmPin,map(Speed, 0, 1023, 0, 150));      //PWM Speed Control   
      analogWrite (MotorBPwmPin,map(Speed, 0, 1023, 0, 150)); 

      // MEASURE POLULU MOTOR SPEED
      ontimeA = pulseIn(MotorAEncoderPin,HIGH, 500000);
      ontimeB = pulseIn(MotorBEncoderPin,HIGH, 500000);
      offtimeA = pulseIn(MotorAEncoderPin,LOW, 500000);
      offtimeB = pulseIn(MotorBEncoderPin,LOW, 500000);
      periodA = ontimeA+offtimeA;
      freqA =   1000000.0/periodA;

      periodB = ontimeB+offtimeB;
      freqB =   1000000.0/periodB;


      // Note : if value is inf (meaning period is 0) then motor powered off or not connected to power source

      if (motorAstuck == 0){
        currentMillis = millis();  //get the current time 
        if (currentMillis - startMillis >= poluluPeriodUnstuck) {
          if (freqA < 1300 || freqB < 1300 || freqA > 5000 || freqB > 5000) {
            digitalWrite(MotorADirectionPin, HIGH);
            digitalWrite(MotorBDirectionPin, LOW);
            motorAstuck = 1;
            startMillis = millis();  //initial start time when motors considered stuck
          }          
        }
      }

      if (motorAstuck == 1){
        currentMillis = millis();  //get the current time 
        if (currentMillis - startMillis >= poluluPeriod) {
            digitalWrite(MotorADirectionPin, LOW);
            digitalWrite(MotorBDirectionPin, HIGH);
            motorAstuck = 0;
            // delay(100); // avoid delays using millis() below :
            startMillis = millis();  //initial start time when motors considered unstuck
        }
      }
  }

  else {
      analogWrite (MotorAPwmPin,0);      // if polulusOn == 0, Stop Polulu motors   
      analogWrite (MotorBPwmPin,0);
  }

}

void SonarSensor(int trigPin,int echoPin) {

  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
  duration = pulseIn(echoPin, HIGH);
  distance = duration/58.2;
}

void sensorAvoidance() {
  // If obstacles detected, override previous motor commands
  bool obstacleLeft = 0;
  bool obstacleRight = 0;

  if ( (cm[2] <= 25 && cm[2]>0) || (cm[1] <= 25 && cm[1]>0) ){
    obstacleLeft = 1;
    digitalWrite(rightMotorDirectionPin, LOW);
    for (int i = 18; i <= 31; i++) {
      leds[i] = CRGB ( 10, 0, 10); // purple
    }

  }
  else {
    obstacleLeft = 0;
    digitalWrite(rightMotorDirectionPin, HIGH);
    for (int i = 18; i <= 31; i++) {
      leds[i] = CRGB ( 0, 0, 10); // purple
    }
  }


  if ( (cm[3] <= 25 && cm[3]>0) || (cm[4] <= 25 && cm[4]>0) ){
    obstacleRight = 1;
    digitalWrite(leftMotorDirectionPin, LOW);
    analogWrite(leftMotorPwmPin, map(abs(left_speed)-40, 0, 198, 26, 230));
    for (int i = 0; i <= 13; i++) {
      leds[i] = CRGB ( 10, 0, 10); // purple
    }
  }
  else {
    obstacleRight = 0;
    digitalWrite(leftMotorDirectionPin, HIGH);
    for (int i = 0; i <= 13; i++) {
      leds[i] = CRGB ( 0, 0, 10); // purple
    }
  }

  // if (obstacleLeft && obstacleRight) {
  //   if (obstacleCounter == 0) {
  //     obstacleStartMillis = millis();
  //   }
  //   obstacleCounter++;
  //   Serial.println(obstacleCounter);
  //   if (obstacleCounter >= 200) {
  //     obstacleStartMillis = millis();
  //     obstacleCurrentMillis = millis();
  //     digitalWrite(leftMotorDirectionPin, LOW);
  //     digitalWrite(leftMotorDirectionPin, LOW);
  //     Serial.println(obstacleCurrentMillis - obstacleStartMillis);
  //     if (obstacleCurrentMillis - obstacleStartMillis >= obstaclePeriod) {
  //         obstacleCounter = 0;
  //     }      
  //   }
  // }
}

void updateMotorCommands() {
  left_speed = forward_speed + angular_speed;
  right_speed = forward_speed - angular_speed;

  if (left_speed < 0) {
    digitalWrite(leftMotorDirectionPin, LOW);
  }
  else {
    digitalWrite(leftMotorDirectionPin, HIGH);
  }
  if (right_speed < 0) {
    digitalWrite(rightMotorDirectionPin, LOW);
  }
  else {
    digitalWrite(rightMotorDirectionPin, HIGH);
  }
  // NOTE : to communicate with ESCON MAXON cntroller, pwm duty cycle must be between 10% and 90%
  analogWrite(leftMotorPwmPin, map(abs(left_speed), 0, 198, 26, 230));
  analogWrite(rightMotorPwmPin , map(abs(right_speed), 0, 198, 26, 230)); // desired speed sent as pwm to ESCON controller
}

void readSerialPort() {

  // Code taken from : https://www.aranacorp.com/en/serial-communication-between-raspberry-pi-and-arduino/
  // Our own protocol:
  // send motor commands as for example "m+08-99" (total string lenght of 7)
  // send unloading command as "u"
  // send emergency STOP command as "s"
  // send polulu command as "p0" or "p1"
  
  msg = "";
  if (Serial.available()) {
    delay(5);
    msg = Serial.readStringUntil('\n');


    if (msg[0] == 'm') { // m for motor commands
      // Serial.println("MOTOR DATA");
      if (msg.length() == 7){
        String msg_forward_speed = msg.substring(1, 4);
        String msg_angular_speed = msg.substring(4,7);
        forward_speed = msg_forward_speed.toInt();
        angular_speed = msg_angular_speed.toInt();

        // Serial.println(forward_speed);
        // Serial.println(angular_speed);
        digitalWrite(leftMotorEnablePin, HIGH);
        digitalWrite(rightMotorEnablePin, HIGH);
      }
    }
    else if (msg[0]=='u'){ // u for duplo unloading
      // Serial.println("UNLOADING");
      digitalWrite(leftMotorEnablePin, LOW);
      digitalWrite(rightMotorEnablePin, LOW);
      moveServo();

    }
    else if (msg[0]=='s'){
      // Serial.println("STOP NOW!!");
      digitalWrite(leftMotorEnablePin, LOW);
      digitalWrite(rightMotorEnablePin, LOW);
    }
    else if (msg[0]=='p'){
      if (msg == "p0") {
        polulusOn = 0;
      }
      else if (msg == "p1") {
        polulusOn = 1;
      }
    }
    else if (msg[0]=='d'){
      sendData();
    }
    Serial.flush();
  }
}

void sendData() {
  //write data
  Serial.print("d"); // print data
  for(uint8_t i = 0; i < ULTRASONIC_NUM; i++) {
    Serial.print(cm[i]);
    Serial.print(" ");
  }
  Serial.println(""); // return to line indicates end of message

}

void moveServo() {

  for (pos = 30; pos <= 145; pos += 1) { // goes from 0 degrees to 180 degrees
    // in steps of 1 degree
    bigAssServo.write(pos);              // tell servo to go to position in variable 'pos'
    delay(15);                       // waits 15 ms for the servo to reach the position
  }
  for (pos = 145; pos >= 30; pos -= 1) { // goes from 180 degrees to 0 degrees
    bigAssServo.write(pos);              // tell servo to go to position in variable 'pos'
    delay(15);                       // waits 15 ms for the servo to reach the position
  }
}

void checkBatteryLevel() {

  // Battery Level Monitoring
  // 100% : >= 830, 75% : >= 798, 50% : >= 757, 25% : >= 716
  batteryLevel = analogRead(A15);
  // Serial.print("BATTERY LEVEL : ");
  // Serial.println(batteryLevel);
  if (batteryLevel >= 830) {

    batteryLedsState[0] = 1;
    batteryLedsState[1] = 1;
    batteryLedsState[2] = 1;
    batteryLedsState[3] = 0;
  }

  if (batteryLevel < 830 && batteryLevel >= 798) {

    batteryLedsState[0] = 1;
    batteryLedsState[1] = 1;
    batteryLedsState[2] = 1;
    batteryLedsState[3] = 0;
  }
  else if (batteryLevel < 798 && batteryLevel >= 756) {

    batteryLedsState[0] = 1;
    batteryLedsState[1] = 1;
    batteryLedsState[2] = 0;
    batteryLedsState[3] = 0;
  }
  else if (batteryLevel < 756 and batteryLevel >= 715) {

    batteryLedsState[0] = 1;
    batteryLedsState[1] = 0;
    batteryLedsState[2] = 0;
    batteryLedsState[3] = 0;
  }

  else if (batteryLevel < 715) {
    // NEED TO WARN RASPBERRY PI
    Serial.println("b");
 
    batteryLedsState[0] = 0;
    batteryLedsState[1] = 0;
    batteryLedsState[2] = 0;
    batteryLedsState[3] = 0;
  }


  for (uint8_t i = 0;  i < 4; i++) {
    if (prevBatteryLedsState[i] != batteryLedsState[i]) {
      if (batteryLedsState[i] == 0) {
        leds[i+14] = CRGB ( 10, 0, 0);
      }
      else {
        leds[i+14] = CRGB ( 0, 10, 0);        
      }
      FastLED.show(); 

      prevBatteryLedsState[i] = batteryLedsState[i];     
    }
  }
}

void checkMotors() {
  speedLeft = analogRead(A8);
  currentLeft = analogRead(A9);
  //Serial.println(speedLeft); // around 800 at max speed
  //Serial.println(currentLeft);  // around 200 when going up ramp, 350 when stuck

}
